﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("Tag")]
    public partial class Tag
    {
        public Tag()
        {
            Hastags = new HashSet<Hastag>();
        }

        [StringLength(40)]
        public string Name { get; set; } = null!;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("TagID", TypeName = "numeric(10, 0)")]
        public decimal TagId { get; set; }

        [InverseProperty("Tag")]
        public virtual ICollection<Hastag> Hastags { get; set; }
    }
}
