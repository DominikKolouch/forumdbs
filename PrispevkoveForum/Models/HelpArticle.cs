﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Keyless]
    public partial class HelpArticle
    {
        [StringLength(100)]
        public string Name { get; set; } = null!;
        public string Text { get; set; } = null!;
    }
}
