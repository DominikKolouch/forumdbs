﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Keyless]
    public partial class AverageAge
    {
        [Column("prumernyVek")]
        public int PrumernyVek { get; set; }
    }
}
