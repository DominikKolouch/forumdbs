﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Keyless]
    public partial class VypisAdministratory
    {
        [StringLength(20)]
        public string Username { get; set; } = null!;
        [StringLength(50)]
        public string? Email { get; set; }
    }
}
