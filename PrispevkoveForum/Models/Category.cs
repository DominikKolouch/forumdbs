﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("Category")]
    public partial class Category
    {
        public Category()
        {
            Threads = new HashSet<Thread>();
        }

        public int Agelimit { get; set; }
        [StringLength(50)]
        public string Name { get; set; } = null!;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("CategoryID", TypeName = "numeric(10, 0)")]
        public decimal CategoryId { get; set; }

        [InverseProperty("Category")]
        public virtual ICollection<Thread> Threads { get; set; }
    }
}
