﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("Hastag")]
    public partial class Hastag
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("HastagID", TypeName = "numeric(10, 0)")]
        public decimal HastagId { get; set; }
        [Column("ArticleID", TypeName = "numeric(10, 0)")]
        public decimal? ArticleId { get; set; }
        [Column("TagID", TypeName = "numeric(10, 0)")]
        public decimal TagId { get; set; }

        [ForeignKey("ArticleId")]
        [InverseProperty("Hastags")]
        public virtual Article? Article { get; set; }
        [ForeignKey("TagId")]
        [InverseProperty("Hastags")]
        public virtual Tag Tag { get; set; } = null!;
    }
}
