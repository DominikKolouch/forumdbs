﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("Chat")]
    public partial class Chat
    {
        public Chat()
        {
            Messages = new HashSet<Message>();
            Users = new HashSet<User>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ChatID", TypeName = "numeric(10, 0)")]
        public decimal ChatId { get; set; }

        [InverseProperty("Chat")]
        public virtual ICollection<Message> Messages { get; set; }
        [InverseProperty("Chat")]
        public virtual ICollection<User> Users { get; set; }
    }
}
