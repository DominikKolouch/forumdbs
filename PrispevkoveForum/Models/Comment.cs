﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("Comment")]
    public partial class Comment
    {
        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }
        [StringLength(1000)]
        public string Text { get; set; } = null!;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("CommentID", TypeName = "numeric(10, 0)")]
        public decimal CommentId { get; set; }
        [Column("ArticleID", TypeName = "numeric(10, 0)")]
        public decimal ArticleId { get; set; }
        [Column("UserID", TypeName = "numeric(10, 0)")]
        public decimal UserId { get; set; }

        [ForeignKey("ArticleId")]
        [InverseProperty("Comments")]
        public virtual Article Article { get; set; } = null!;
        [ForeignKey("UserId")]
        [InverseProperty("Comments")]
        public virtual User User { get; set; } = null!;
    }
}
