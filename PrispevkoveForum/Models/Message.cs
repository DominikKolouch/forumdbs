﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("Message")]
    public partial class Message
    {
        [StringLength(20)]
        public string Autor { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }
        [StringLength(1000)]
        public string Text { get; set; } = null!;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("MessageID", TypeName = "numeric(10, 0)")]
        public decimal MessageId { get; set; }
        [Column("ChatID", TypeName = "numeric(10, 0)")]
        public decimal ChatId { get; set; }

        [ForeignKey("ChatId")]
        [InverseProperty("Messages")]
        public virtual Chat Chat { get; set; } = null!;
    }
}
