﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("Article")]
    public partial class Article
    {
        public Article()
        {
            Comments = new HashSet<Comment>();
            Files = new HashSet<File>();
            Hastags = new HashSet<Hastag>();
            Users = new HashSet<User>();
        }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Lastupdate { get; set; }
        [StringLength(100)]
        public string Name { get; set; } = null!;
        public string Text { get; set; } = null!;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ArticleID", TypeName = "numeric(10, 0)")]
        public decimal ArticleId { get; set; }
        [Column("UserID", TypeName = "numeric(10, 0)")]
        public decimal UserId { get; set; }
        [Column("ThreadID", TypeName = "numeric(10, 0)")]
        public decimal ThreadId { get; set; }

        [ForeignKey("ThreadId")]
        [InverseProperty("Articles")]
        public virtual Thread Thread { get; set; } = null!;
        [ForeignKey("UserId")]
        [InverseProperty("Articles")]
        public virtual User User { get; set; } = null!;
        [InverseProperty("Article")]
        public virtual ICollection<Comment> Comments { get; set; }
        [InverseProperty("Article")]
        public virtual ICollection<File> Files { get; set; }
        [InverseProperty("Article")]
        public virtual ICollection<Hastag> Hastags { get; set; }

        [ForeignKey("ArticleId")]
        [InverseProperty("ArticlesNavigation")]
        public virtual ICollection<User> Users { get; set; }
    }
}
