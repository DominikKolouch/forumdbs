﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("File")]
    public partial class File
    {
        [StringLength(200)]
        public string Name { get; set; } = null!;
        [StringLength(5)]
        public string Type { get; set; } = null!;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("FileID", TypeName = "numeric(10, 0)")]
        public decimal FileId { get; set; }
        [Column("ArticleID", TypeName = "numeric(10, 0)")]
        public decimal ArticleId { get; set; }

        [ForeignKey("ArticleId")]
        [InverseProperty("Files")]
        public virtual Article Article { get; set; } = null!;
    }
}
