﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("User")]
    [Microsoft.EntityFrameworkCore.Index(nameof(Username), Name = "idx_Username")]
    public partial class User
    {
        public User()
        {
            Articles = new HashSet<Article>();
            Comments = new HashSet<Comment>();
            ArticlesNavigation = new HashSet<Article>();
        }

        public bool Online { get; set; }
        [StringLength(200)]
        public string Password { get; set; } = null!;
        [StringLength(20)]
        public string Username { get; set; } = null!;
        [StringLength(50)]
        public string? Email { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("UserID", TypeName = "numeric(10, 0)")]
        public decimal UserId { get; set; }
        [Column("ChatID", TypeName = "numeric(10, 0)")]
        public decimal ChatId { get; set; }
        [Column("UsertypeID", TypeName = "numeric(10, 0)")]
        public decimal UsertypeId { get; set; }

        [ForeignKey("ChatId")]
        [InverseProperty("Users")]
        public virtual Chat Chat { get; set; } = null!;
        [ForeignKey("UsertypeId")]
        [InverseProperty("Users")]
        public virtual Usertype Usertype { get; set; } = null!;
        [InverseProperty("User")]
        public virtual ICollection<Article> Articles { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<Comment> Comments { get; set; }

        [ForeignKey("UserId")]
        [InverseProperty("Users")]
        public virtual ICollection<Article> ArticlesNavigation { get; set; }
    }
}
