﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("Usertype")]
    public partial class Usertype
    {
        public Usertype()
        {
            Users = new HashSet<User>();
        }

        [StringLength(20)]
        public string Type { get; set; } = null!;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("UsertypeID", TypeName = "numeric(10, 0)")]
        public decimal UsertypeId { get; set; }

        [InverseProperty("Usertype")]
        public virtual ICollection<User> Users { get; set; }
    }
}
