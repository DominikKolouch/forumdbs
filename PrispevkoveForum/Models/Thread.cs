﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PrispevkoveForum.Models
{
    [Table("Thread")]
    public partial class Thread
    {
        public Thread()
        {
            Articles = new HashSet<Article>();
        }

        [StringLength(1000)]
        public string? Description { get; set; }
        [StringLength(40)]
        public string Name { get; set; } = null!;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ThreadID", TypeName = "numeric(10, 0)")]
        public decimal ThreadId { get; set; }
        [Column("CategoryID", TypeName = "numeric(10, 0)")]
        public decimal CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        [InverseProperty("Threads")]
        public virtual Category Category { get; set; } = null!;
        [InverseProperty("Thread")]
        public virtual ICollection<Article> Articles { get; set; }
    }
}
