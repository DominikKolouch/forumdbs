﻿var TRange=null;

function findString(str) {
    var strFound;
    if (window.find) {

        strFound = self.find(str);
        if (!strFound) {
            strFound = self.find(str, 0, 1);
            while (self.find(str, 0, 1)) continue;
        }
    }
}