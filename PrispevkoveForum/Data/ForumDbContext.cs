﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using PrispevkoveForum.Models;
using PrispevkoveForum.Models;
using File = PrispevkoveForum.Models.File;
using Thread = PrispevkoveForum.Models.Thread;

namespace PrispevkoveForum.Data
{
    public partial class ForumDbContext : DbContext
    {
        public ForumDbContext()
        {
        }

        public ForumDbContext(DbContextOptions<ForumDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Article> Articles { get; set; } = null!;
        public virtual DbSet<AverageAge> AverageAges { get; set; } = null!;
        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<Chat> Chats { get; set; } = null!;
        public virtual DbSet<Comment> Comments { get; set; } = null!;
        public virtual DbSet<File> Files { get; set; } = null!;
        public virtual DbSet<Hastag> Hastags { get; set; } = null!;
        public virtual DbSet<HelpArticle> HelpArticles { get; set; } = null!;
        public virtual DbSet<Message> Messages { get; set; } = null!;
        public virtual DbSet<Tag> Tags { get; set; } = null!;
        public virtual DbSet<Thread> Threads { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<Usertype> Usertypes { get; set; } = null!;
        public virtual DbSet<VypisAdministratory> VypisAdministratories { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=localhost; Database=forumDatabase;Password=UHK.PKJL178a; User Id=sa; MultipleActiveResultSets=true; Integrated security=false");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Article>(entity =>
            {
                entity.Property(e => e.ArticleId).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Thread)
                    .WithMany(p => p.Articles)
                    .HasForeignKey(d => d.ThreadId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Article_Thread");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Articles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Article_User");
            });

            modelBuilder.Entity<AverageAge>(entity =>
            {
                entity.ToView("averageAge");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.CategoryId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Chat>(entity =>
            {
                entity.Property(e => e.ChatId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.Property(e => e.CommentId).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.ArticleId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Comment_Article");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Comment_User");
            });

            modelBuilder.Entity<File>(entity =>
            {
                entity.Property(e => e.FileId).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.Files)
                    .HasForeignKey(d => d.ArticleId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_File_Article");
            });

            modelBuilder.Entity<Hastag>(entity =>
            {
                entity.Property(e => e.HastagId).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.Hastags)
                    .HasForeignKey(d => d.ArticleId)
                    .HasConstraintName("FK_hasTag_Article");

                entity.HasOne(d => d.Tag)
                    .WithMany(p => p.Hastags)
                    .HasForeignKey(d => d.TagId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_hasTag_Tag");
            });

            modelBuilder.Entity<HelpArticle>(entity =>
            {
                entity.ToView("HelpArticles");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.Property(e => e.MessageId).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Chat)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.ChatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Message_Chat");
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.Property(e => e.TagId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Thread>(entity =>
            {
                entity.Property(e => e.ThreadId).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Threads)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Thread_Category");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.UserId).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Chat)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.ChatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Chat");

                entity.HasOne(d => d.Usertype)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.UsertypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Usertype");

                entity.HasMany(d => d.ArticlesNavigation)
                    .WithMany(p => p.Users)
                    .UsingEntity<Dictionary<string, object>>(
                        "Like",
                        l => l.HasOne<Article>().WithMany().HasForeignKey("ArticleId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_Like_Article"),
                        r => r.HasOne<User>().WithMany().HasForeignKey("UserId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_Like_User"),
                        j =>
                        {
                            j.HasKey("UserId", "ArticleId");

                            j.ToTable("Like");

                            j.IndexerProperty<decimal>("UserId").HasColumnType("numeric(10, 0)").HasColumnName("UserID");

                            j.IndexerProperty<decimal>("ArticleId").HasColumnType("numeric(10, 0)").HasColumnName("ArticleID");
                        });
            });

            modelBuilder.Entity<Usertype>(entity =>
            {
                entity.Property(e => e.UsertypeId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<VypisAdministratory>(entity =>
            {
                entity.ToView("VypisAdministratory");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
