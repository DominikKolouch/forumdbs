using System.Globalization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;

namespace PrispevkoveForum.Services;

public class CustomUserStore : IUserPasswordStore<User>, IDisposable, IUserEmailStore<User>, IUserRoleStore<User>
{
    private readonly ForumDbContext _context;
    private IPasswordHasher<User> _passwordHasher;
    
    public CustomUserStore(ForumDbContext context, IPasswordHasher<User> passwordHasher)
    {
        _context = context;
        _passwordHasher = passwordHasher;
    }
    
    public void Dispose()
    {
       this._context.Dispose(); 
    }

    public async Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        if (user == null) throw new ArgumentNullException(nameof(user));

        return await Task.Run(() => user.UserId.ToString(CultureInfo.InvariantCulture), cancellationToken);
    }

    public async Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        if (user == null) throw new ArgumentNullException(nameof(user));

        return await Task.Run(() => user.Username, cancellationToken);
    }

    public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        if (user == null) throw new ArgumentNullException(nameof(user));
        user.Username = userName;

        return Task.CompletedTask;
    }

    public async Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        if (user == null) throw new ArgumentNullException(nameof(user));

        return await Task.Run(() => user.Username.ToUpper(), cancellationToken);
    }

    public Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        if (user == null) throw new ArgumentNullException(nameof(user));
        user.Username = normalizedName.ToLower();

        return Task.CompletedTask;
    }

    public async Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
    {
        var result = await this._context.Users.AddAsync(user, cancellationToken);

        await this._context.SaveChangesAsync(cancellationToken);
        
        if (result is null)
            return IdentityResult.Failed(new IdentityError() { Code = "120", Description = "Cannot Create User!" });
                
        return IdentityResult.Success;
    }

    public async Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
    {
        var result = this._context.Users.Update(user);
        
        await this._context.SaveChangesAsync(cancellationToken);
        
        if (result is null)
            return await Task.FromResult(IdentityResult.Failed(new IdentityError() { Code = "120", Description = "Cannot Create User!" }));
                
        return await Task.FromResult(IdentityResult.Success);
    }

    public async Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
    {
        var result = this._context.Users.Remove(user);
        
        await this._context.SaveChangesAsync(cancellationToken);
        
        if (result is null)
            return await Task.FromResult(IdentityResult.Failed(new IdentityError() { Code = "120", Description = "Cannot Create User!" }));
                
        return await Task.FromResult(IdentityResult.Success);
    }

    public async Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
    {
        return await this._context.Users
            .FirstOrDefaultAsync(u => u.UserId == decimal.Parse(userId), cancellationToken: cancellationToken);
    }

    public async Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
    {
        var user = await this._context.Users.FirstOrDefaultAsync(u => u.Email != null && u.Email.ToUpper() == normalizedUserName,
            cancellationToken: cancellationToken);

        return user;
    }

    public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        if (user == null) throw new ArgumentNullException(nameof(user));
        user.Password = passwordHash;

        return Task.CompletedTask;
    }

    public async Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        if (user == null) throw new ArgumentNullException(nameof(user));

        return await Task.Run(() => user.Password, cancellationToken);
    }

    public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        if (user == null) throw new ArgumentNullException(nameof(user));

        return Task.FromResult(!string.IsNullOrWhiteSpace(user.Password));
    }

    public Task SetEmailAsync(User user, string email, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        if (user == null) throw new ArgumentNullException(nameof(user));
        user.Email = email;

        return Task.CompletedTask;  
    }

    public Task<string> GetEmailAsync(User user, CancellationToken cancellationToken)
    {
        return Task.FromResult(user.Email ?? "");
    }

    public Task<bool> GetEmailConfirmedAsync(User user, CancellationToken cancellationToken)
    {
        return Task.FromResult(true);
    }

    public Task SetEmailConfirmedAsync(User user, bool confirmed, CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    public async Task<User> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
    {
        var user = await this._context.Users.FirstOrDefaultAsync(u => u.Email != null && u.Email.ToUpper() == normalizedEmail,
            cancellationToken: cancellationToken);

        return user; 
    }

    public Task<string> GetNormalizedEmailAsync(User user, CancellationToken cancellationToken)
    {
        if (user.Email is null)
            throw new Exception();
        
        return Task.FromResult(user.Email.ToUpper());
    }

    public Task SetNormalizedEmailAsync(User user, string normalizedEmail, CancellationToken cancellationToken)
    {
        user.Email = normalizedEmail.ToLower();

        return Task.CompletedTask;
    }

    public async Task AddToRoleAsync(User user, string roleName, CancellationToken cancellationToken)
    {
        var role = await _context.Usertypes
            .Include(x => x.Users)
            .FirstOrDefaultAsync(x => x.Type == roleName, cancellationToken);
        
        role.Users.Add(user);

        _context.Usertypes.Update(role);
        
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task RemoveFromRoleAsync(User user, string roleName, CancellationToken cancellationToken)
    {
        
        var role = await _context.Usertypes
            .Include(x => x.Users)
            .FirstOrDefaultAsync(x => x.Type == roleName, cancellationToken);
        
        role.Users.Remove(user);

        _context.Usertypes.Update(role);
        
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task<IList<string>> GetRolesAsync(User user, CancellationToken cancellationToken)
    {
        return await _context.Usertypes
            .Where(x => x.Users.Contains(user))
            .Select(x => x.Type)
            .ToListAsync(cancellationToken);
    }

    public async Task<bool> IsInRoleAsync(User user, string roleName, CancellationToken cancellationToken)
    {
        var role = await _context.Usertypes
            .Include(x => x.Users)
            .FirstOrDefaultAsync(x => x.Type == roleName, cancellationToken);

        return role.Users.Contains(user);
    }

    public async Task<IList<User>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
    {
        var role = await _context.Usertypes
            .Include(x => x.Users)
            .FirstOrDefaultAsync(x => x.Type == roleName, cancellationToken);

        return role.Users.ToList();
    }
}