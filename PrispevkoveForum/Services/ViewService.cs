﻿using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using Thread = PrispevkoveForum.Models.Thread;

namespace PrispevkoveForum.Services
{
    public class ViewService
    {
        public ViewService(ForumDbContext db)
        {
            _db = db;
        }
        private readonly ForumDbContext _db;

        public async Task<List<VypisAdministratory>> GetAdmins()
        {
            return await _db.VypisAdministratories.ToListAsync();
        }

        public async Task<AverageAge> GetAvrgAge()
        {
            return await _db.AverageAges.FirstOrDefaultAsync();
        }

        public async Task<List<HelpArticle>> GetHelpArticles()
        {
            return await _db.HelpArticles.ToListAsync();
        }
    }
}
