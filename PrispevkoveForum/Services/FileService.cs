﻿using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using File = PrispevkoveForum.Models.File;
using Thread = PrispevkoveForum.Models.Thread;

namespace PrispevkoveForum.Services
{
    public class FileService
    {
        public FileService(ForumDbContext _db)
        {
            this._db = _db;
        }
        private readonly ForumDbContext _db;

        public async Task AddFile(File file)
        {
            await _db.Files.AddAsync(file);
            await _db.SaveChangesAsync();
        }

        public async Task<List<File>> GetFiles(decimal articleID)
        {
            return await _db.Files.Where(x => x.ArticleId == articleID).ToListAsync();
        }
    }
}
