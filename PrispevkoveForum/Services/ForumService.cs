﻿using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using Thread = PrispevkoveForum.Models.Thread;

namespace PrispevkoveForum.Services
{
    //napsat service pak ji registrovat v program cs

    public class ForumService
    {
        public ForumService(ForumDbContext db)
        {
            _db = db;
        }

        private readonly ForumDbContext _db; //_readonly class proměnná


        public async Task<List<Tag>> GetTags()
        {
            return await _db.Tags.ToListAsync();
        }



    }
}
