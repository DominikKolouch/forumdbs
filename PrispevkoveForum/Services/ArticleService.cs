﻿using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using Thread = PrispevkoveForum.Models.Thread;

namespace PrispevkoveForum.Services
{
    public class ArticleService
    {
        public ArticleService(ForumDbContext db)
        {
            _db = db;
        }

        private readonly ForumDbContext _db; //_readonly class proměnná

        public async Task<Article> GetArticle(int ArticleId)
        {
            return await _db.Articles.FirstOrDefaultAsync(x => x.ArticleId == ArticleId);
        }

        public async Task<List<Article>> GetArticles(int ThreadId)
        {
            return await _db.Articles.
                Where(x => x.ThreadId == ThreadId).
                Include(x => x.Comments).
                OrderByDescending(x=>x.Created).
                ToListAsync();
        }
        public async Task RemoveArticle(decimal articleId)
        {
            var cat = await _db.Articles
                .Include(a => a.Files)
                .Include(a => a.Hastags)
                .Include(a => a.Comments)
                .FirstOrDefaultAsync(x => x.ArticleId == articleId);
            if (cat != null)
                _db.Articles.Remove(cat);
            await _db.SaveChangesAsync();
        }


        public async Task UpdateArticle(Article articleNew, decimal articleId)
        {
            var article = await _db.Articles.FirstOrDefaultAsync(a => a.ArticleId == articleId);
            article.Name = articleNew.Name;
            article.Text = articleNew.Text;
            await _db.SaveChangesAsync();
        }

        public async Task CreateArticle(Article article)
        {
            await _db.Articles.AddAsync(article);
            await _db.SaveChangesAsync();
            //_db.Database.ExecuteSqlRaw($"EXEC PridatClanek {0}, {1},{2}, {3}", article.Name,article.Text,8,article.ThreadId);
            
            
            await _db.SaveChangesAsync();
        }

    }
}
