﻿using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using Thread = PrispevkoveForum.Models.Thread;

namespace PrispevkoveForum.Services
{
    public class MessageService
    {
        public MessageService(ForumDbContext db)
        {
            _db = db;
        }

        private readonly ForumDbContext _db; //_readonly class proměnná

        public async Task AddMessage(Message message)
        {
            await _db.Messages.AddAsync(message);
            await _db.SaveChangesAsync();
        }
        public async Task<List<Message>> GetMessages()
        {
            return await _db.Messages.
                OrderByDescending(x => x.Created).
                ToListAsync();
        }

    }
}
