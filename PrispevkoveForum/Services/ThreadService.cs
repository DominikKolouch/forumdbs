﻿using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using Thread = PrispevkoveForum.Models.Thread;

namespace PrispevkoveForum.Services
{
    public class ThreadService
    {
        public ThreadService(ForumDbContext db)
        {
            _db = db;
        }

        private readonly ForumDbContext _db; //_readonly class proměnná

        public async Task<List<Thread>> GetThreads(int CategoryId)
        {
            return await _db.Threads.
                Where(x => x.CategoryId == CategoryId).
                Include(x => x.Articles).
                OrderByDescending(x=>x.ThreadId).
                ToListAsync();
        }

        public async Task CreateThread(Thread thread)
        {
            await _db.Threads.AddAsync(thread);
            await _db.SaveChangesAsync();
        }
    }
}
