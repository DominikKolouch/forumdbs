using System.Globalization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;

namespace PrispevkoveForum.Services;

public class CustomRoleStore : IRoleStore<Usertype>, IDisposable
{
    private readonly ForumDbContext _context;
    public CustomRoleStore(ForumDbContext context)
    {
        _context = context;
    }
    
    public void Dispose()
    {
        this._context.Dispose();
    }

    public async Task<IdentityResult> CreateAsync(Usertype role, CancellationToken cancellationToken)
    {
        var result = await _context.Usertypes.AddAsync(role, cancellationToken);
        
        await this._context.SaveChangesAsync(cancellationToken);
        
        if (result is null)
            return IdentityResult.Failed(new IdentityError() { Code = "120", Description = "Cannot Create Role!" });
                
        return IdentityResult.Success;
    }

    public async Task<IdentityResult> UpdateAsync(Usertype role, CancellationToken cancellationToken)
    {
        var result = _context.Usertypes.Update(role);
        
        await this._context.SaveChangesAsync(cancellationToken);
        
        if (result is null)
            return IdentityResult.Failed(new IdentityError() { Code = "120", Description = "Cannot Create Role!" });
                
        return IdentityResult.Success;
    }

    public async Task<IdentityResult> DeleteAsync(Usertype role, CancellationToken cancellationToken)
    {
        var result = _context.Usertypes.Remove(role);
        
        await this._context.SaveChangesAsync(cancellationToken);
        
        if (result is null)
            return IdentityResult.Failed(new IdentityError() { Code = "120", Description = "Cannot Create Role!" });
                
        return IdentityResult.Success;
    }

    public Task<string> GetRoleIdAsync(Usertype role, CancellationToken cancellationToken)
    {
        return Task.FromResult(role.UsertypeId.ToString(CultureInfo.InvariantCulture));
    }

    public Task<string> GetRoleNameAsync(Usertype role, CancellationToken cancellationToken)
    {
        return Task.FromResult(role.Type);
    }

    public async Task SetRoleNameAsync(Usertype role, string roleName, CancellationToken cancellationToken)
    {
        var item = this._context.Usertypes.FirstOrDefault(x => x.UsertypeId == role.UsertypeId);
        item.Type = roleName;

        this._context.Usertypes.Update(item);

        await this._context.SaveChangesAsync(cancellationToken);
    }

    public Task<string> GetNormalizedRoleNameAsync(Usertype role, CancellationToken cancellationToken)
    {
        return Task.FromResult(role.Type.ToUpperInvariant());
    }

    public async Task SetNormalizedRoleNameAsync(Usertype role, string normalizedName, CancellationToken cancellationToken)
    {
        var item = this._context.Usertypes.FirstOrDefault(x => x.UsertypeId == role.UsertypeId);
        item.Type = role.Type;

        this._context.Usertypes.Update(item);

        await this._context.SaveChangesAsync(cancellationToken);
    }

    public Task<Usertype> FindByIdAsync(string roleId, CancellationToken cancellationToken)
    {
        return _context.Usertypes.FirstOrDefaultAsync(u => u.UsertypeId == decimal.Parse(roleId), cancellationToken);
    }

    public Task<Usertype> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
    {
        return _context.Usertypes.FirstOrDefaultAsync(u => u.Type.ToUpperInvariant() == normalizedRoleName, cancellationToken);
    }
}