﻿using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using Thread = PrispevkoveForum.Models.Thread;

namespace PrispevkoveForum.Services
{


    public class CommentService
    {
        public CommentService(ForumDbContext db)
        {
            _db = db;
        }

        private readonly ForumDbContext _db; //_readonly class proměnná

        public async Task<List<Comment>> GetComments(decimal ArticleId)
        {
            return await _db.Comments.Include(x => x.User).Where(x => x.ArticleId == ArticleId)
                .OrderByDescending(x => x.Created).ToListAsync();
        }

        public async Task AddComment(Comment comment)
        {
            await _db.Comments.AddAsync(comment);
            await _db.SaveChangesAsync();
        }
    }
}