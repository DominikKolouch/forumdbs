﻿using Microsoft.EntityFrameworkCore;
using PrispevkoveForum.Data;
using PrispevkoveForum.Models;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Thread = PrispevkoveForum.Models.Thread;

namespace PrispevkoveForum.Services
{
    public class CategoryService
    {
        public CategoryService(ForumDbContext _db)
        {
            this._db = _db;
        }
        private readonly ForumDbContext _db;

        public async Task<List<Category>> GetCategories()
        {
            return await _db.Categories.
                Include(x => x.Threads).
                OrderByDescending(x=>x.CategoryId).
                ToListAsync();
        }
        public async Task AddCategory(Category category)
        {
            await _db.Categories.AddAsync(category);
            await _db.SaveChangesAsync();
        }
        public async Task RemoveCategory(decimal CategoryID)
        {
            var cat = await _db.Categories
                .Include(c => c.Threads)
                .ThenInclude(t => t.Articles)
                .ThenInclude(f => f.Files)
                .Include(c => c.Threads)
                .ThenInclude(t => t.Articles)
                .ThenInclude(f => f.Hastags)
                .Include(c => c.Threads)
                .ThenInclude(t => t.Articles)
                .ThenInclude(f => f.Comments)
                .FirstOrDefaultAsync(x => x.CategoryId == CategoryID);
            if (cat != null)
                _db.Categories.Remove(cat);
            await _db.SaveChangesAsync();
        }

        public async Task GetView()
        {

        }
    }
}
