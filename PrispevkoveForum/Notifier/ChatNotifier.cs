﻿using PrispevkoveForum.Models;

namespace PrispevkoveForum.Notifier
{
    public class ChatNotifier
    {
        public Action<Message> OnMessage
        {
            get; set;
        }

        public void AddMessage(Message message)
        {
            OnMessage?.Invoke(message);
        }
    }
}
